#!/bin/bash
USERNAME=$(op item get "Mulesoft (GitLab)" --fields label=username)
PASSWORD=$(op item get "Mulesoft (GitLab)" --fields label=password)
MESSAGE="Deployed from desktop on $(date)"
docker run -v $(pwd):/app -e USERNAME="${USERNAME}" -e PASSWORD="${PASSWORD}" \
   -e MESSAGE="${MESSAGE}" -it maven:3.8.2-jdk-8 app/deploy.sh

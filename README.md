# Mulesoft

Prototype deploying a "Mule" to Mulesoft Anypoint. WIP.

Based on [Hello Mule](https://github.com/mulesoft-developers/getting-started-hello-mule)

1. Set up a Mulesoft account
1. Add the username and password to 1Password (see `deploy-from-desktop.sh`)
1. Start Docker Desktop
1. Run `./deploy-from-desktop.sh`

To try out Maven ideas:

```bash
docker run -v $(pwd):/app -it maven:3.8.2-jdk-8 /bin/bash
```

Some references

- [Maven POM.xml](https://books.sonatype.com/mvnref-book/reference/pom-relationships-sect-pom-syntax.html)
- [Configure HTTP listener source](https://docs.mulesoft.com/http-connector/latest/http-listener-ref)
- [Create a Mule Application Quickly from a Template](https://docs.mulesoft.com/design-center/import-template)
- [Deploy Applications to CloudHub Using the Mule Maven Plugin](https://docs.mulesoft.com/mule-runtime/latest/deploy-to-cloudhub)
- [Deploying mule application to Anypoint Platform using GITLAB — CI/CD](https://medium.com/@pratyushm49/mulesoft-anypoint-ci-cd-using-gitlab-41b4d2479f0c)
- [Mule Runtime Engine Overview](https://docs.mulesoft.com/mule-runtime/latest/)
- [Runtime Manager](https://anypoint.mulesoft.com/cloudhub/#/console/home/applications)

Prototype

[Mulesoft app](https://gitlab.com/fpotter-gitlab/sandbox/mulesoft) deployed using GitLab CICD and [running](http://fpotterhellomule.us-e2.cloudhub.io/hellomule) (note HTTP only).

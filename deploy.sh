  #!/bin/bash
  cd app
#   mvn help:effective-pom -Denvironment=Sandbox -DappName=MuleInvaders \
#     -Danypoint.username=$USERNAME -Danypoint.password=password -DmuleVersion=4.5 \
#     -Dworkers=1 -DvCore=Micro -Dmaven.repo.local=./.m2/repository > .effective-pom.xml
  sed "s/\[\[message\]\]/${MESSAGE}/g" src/main/mule/hellomule.xml.template > src/main/mule/hellomule.xml
  mvn package deploy -DmuleDeploy -Denvironment=Sandbox -DappName=FPotterHelloMule \
    -Danypoint.username=$USERNAME -Danypoint.password=$PASSWORD -DmuleVersion=4.5 \
    -Dworkers=1 -DvCore=Micro -Dmaven.repo.local=./.m2/repository